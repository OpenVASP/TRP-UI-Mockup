# TRP UI Mockup
This is a repository for non-functional mockups of potential UI/UX for use with https://gitlab.com/OpenVASP/travel-rule-protocol/

Note this repository is less formally maintained than the main Travel Rule Protocol repository and is used primarily for sharing ideas about how UI/UX could work with TRP. As such it is likely that the ideas in this project may be out of date or different to the thoughts and specifications in the main repository which should be given precedence.
